import React,{Component} from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default class NativeView extends Component {
    render(){
        return(
            <View style={CustomViewStyles.container}>
                <View style= {{backgroundColor: 'blue', flex:0.3}}></View>
                <View style={{backgroundColor: 'red', flex:0.5}}></View>
                <Text style={{fontSize: 30}}>This is Customa View Implemntation</Text>
            </View>
        );
    }
}

const CustomViewStyles = StyleSheet.create({
    container:{
        flex:1, //This defines amount of space it should cover of its parent view, values from 0 to 1
        flexDirection: 'row', // This decides weather flex direction as row or column, 
                                 // if it's "row" then height will be 100% and width will devide as per flex % and 
                                 // if it's "column" then width will be 100% and height will devide as per flex %  
        height : "100%",
        width: "80%",
        backgroundColor: "#5ead97",
        marginTop: 50
    }
})