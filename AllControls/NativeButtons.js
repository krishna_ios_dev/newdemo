import React, { Component } from 'react';
import { Alert, AppRegistry, Button, StyleSheet, View } from 'react-native';

export default class NativeButtons extends Component {
    onPressButton(id) {
        Alert.alert('You Clicked Button! with id' + id)
    }

    onPressNumber() {
        Alert.alert('Kya aap is number par se sampark karna chahenge?')
    }

    onPressEnabledButton() {
        Alert.alert('How dare you touched me? You developer!!!! Now close me.')
    }

    render() {
        return (
            <View style={NativeButtonStyle.buttonContainer}>
                <View style={NativeButtonStyle.buttonContainer}>
                    <Button ref={ref => this.button = ref} onPress={() => this.onPressButton(this.button.props.id)}
                        title="Action with Referance"
                        id="1" />

                    <Button onPress={() => this.onPressButton(this.button.props.testID)}
                        title="Test Id Action"
                        id="1" 
                        testID="TetstId"/>
                </View>

                <View style={NativeButtonStyle.buttonContainer}>
                    <Button onPress={this.onPressNumber}
                        color='blue'
                        title="564564564" />
                </View>

                <View style={NativeButtonStyle.multipButtonContiner}>
                    <Button onPress={this.onPressButton}
                        color='red'
                        title="I am disabled"
                        disabled={true} />

                    <Button onPress={this.onPressEnabledButton}
                        color='black'
                        title="You can click me" />
                </View>
            </View>
        );
    }
}

const NativeButtonStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    buttonContainer: {
        margin: 20
    },
    multipButtonContiner: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})
