import React, { Component } from 'react';  
import { StyleSheet,View } from 'react-native';  
  
export default class FlexJustifyContent extends Component {  
    render() {  
        return (  
            <View style={FlexJustifyContentStyles.container}>  
                <View style={FlexJustifyContentStyles.powderblue} />  
                <View style={FlexJustifyContentStyles.skyblue} />  
                <View style={FlexJustifyContentStyles.steelblue} />  
            </View>  
        );  
    }  
}  
const FlexJustifyContentStyles = StyleSheet.create({  
    container:{  
        flex: 1,  
        flexDirection: 'column', // set elements horizontally`.  
        justifyContent: 'center',  // flex-start, flex-end, space-between , space-around
    },  
    powderblue:{  
        width: 60,  
        height: 60,  
        backgroundColor: 'powderblue'  
    },  
    skyblue:{  
        width: 60,  
        height: 60,  
        backgroundColor: 'skyblue',  
    },  
    steelblue:{  
        width: 60,  
        height: 60,  
        backgroundColor: 'steelblue',  
    }  
})  