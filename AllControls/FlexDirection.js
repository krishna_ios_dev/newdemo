import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

export default class FlexDirection extends Component {
    render() {
        return (
            <View style={FlexDirectionStyle.container}>
                <View style={FlexDirectionStyle.powderBlue} />
                <View style={FlexDirectionStyle.skyBlue} />
                <View style={FlexDirectionStyle.steelBlue} />
            </View>
        );
    }
}

const FlexDirectionStyle = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 50
    },
    powderBlue: {
        width: 60,
        height: 60,
        backgroundColor: 'powderblue'
    },
    skyBlue: {
        width: 60,
        height: 60,
        backgroundColor: 'skyblue'
    },
    steelBlue: {
        width: 60,
        height: 60,
        backgroundColor: 'steelblue'
    }
})
