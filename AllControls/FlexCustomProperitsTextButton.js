import React, { Component } from "react";
import { StyleSheet, TextInput, View, Button } from "react-native";

export default class FlexCustomProperitsTextButton extends Component {

    state = {
        placename: '',
        places: []
    };

    inputInTextEditChanges = val => {
        this.setState({
            placename: val,
            places : this.state.places.concat(val)
        })
    };

    submitButtonProessed = () => {
        alert("Submit Button Pressed and the text is:\n" + this.state.placename + "\n\nAnd Complete txt array is:\n"+ this.state.places)
    };

    render() {
        return (
            <View>
                <TextInput
                    placeholder="Enter your details here"
                    onChangeText={this.inputInTextEditChanges}
                    style={FlexCustomProperitsTextButtonStyles.container}
                />

                <Button
                    title="Submit"
                    onPress={this.submitButtonProessed}
                />
                
            </View>
        );
    }

}

const FlexCustomProperitsTextButtonStyles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 26,
        marginTop: 100,
        color: 'blue',
        borderWidth: 2,
        borderColor: 'skyblue',
        backgroundColor: "#fff",
        justifyContent: "flex-start"
    }
})
