import React, { Component } from 'react';

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default class NativeStates extends Component {

    state = {
        textState: 'This is state text control it will change when it is clicked',
        password: String,
        isPassWordVisible: Boolean,
        toggleText: String
    }

    updatedState = () => this.setState({ textState: 'This is updated text in updated state' })

    constructor(pros) {
        super(pros);
        this.state = {
            password: '',
            isPassWordVisible: true,
            toggleText: 'Show'
        }
    }

    handleToggle = () => {
        const { isPassWordVisible } = this.state

        if (isPassWordVisible) {
            this.setState({ isPassWordVisible: false });
            this.setState({ toggleText: 'Hide' });
        } else {
            this.setState({ isPassWordVisible: true });
            this.setState({ toggleText: 'Show' })
        }
    }

    render() {
        return (
            <View style={NativeStatesStyles.container}>

                <TextInput secureTextEntry={this.state.isPassWordVisible} style={{ width: "100%", height: 50, backgroundColor: '#a7a6a9', color: 'white', fontSize: 20 }}></TextInput>
                <TouchableOpacity onPress={this.handleToggle}>
                    <Text style={{ fontSize: 20 }}>{this.state.toggleText}</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const NativeStatesStyles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        width: "100%",
        height: "100%",
        backgroundColor: "#5ead97",
        marginTop: 50,
    
    }
})

