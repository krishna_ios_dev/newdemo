
import React, { Component } from 'react';
import { Text, Alert, Button, View, StyleSheet, TextInput, TouchableOpacity, Image, ImageBackground } from 'react-native';

export default class ButtonGrid extends Component {

    render() {
        return (
            <View style={styles.container}>

                <ImageBackground
                    source={require('../resources/banner_k.png')}
                    style={styles.imageWrapper}>

                    <Text style={styles.buttonText} > Select Your Favourite Social Media Platform</Text>

                    <View style={styles.multipButtonContiner}>
                        <TouchableOpacity
                            onPress={() => { alert("handler here") }}>
                            <Image style={styles.btnimageWrapper} source={require('../resources/pintrest.png')} ></Image>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => { alert("handler here") }}>
                            <Image style={styles.btnimageWrapper} source={require('../resources/insta_logo.png')} ></Image>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.multipButtonContiner}>
                        <TouchableOpacity
                            onPress={() => { alert("handler here") }}>
                            <Image style={styles.btnimageWrapper} source={require('../resources/Snapchat-Splash.png')} ></Image>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => { alert("handler here") }}>
                            <Image style={styles.btnimageWrapper} source={require('../resources/Splash-Whatsapp-Icon-Png.png')} ></Image>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.bottomcontainer}>

                        <View style={styles.buttonContainer}>
                            <Button
                                title="Home"
                                style={{ textAlign: 'center' }}
                                color="white"
                                onPress={() => Alert.alert('Simple Button pressed')}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button style={{ textAlign: 'center', color:'white' }} title="About Us" color="white" fontWeight="bold"/>
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button style={{ textAlign: 'center' }} title="Settings" color="white" />
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    titleText: {
        fontSize: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        backgroundColor: 'steelblue',
        width: "80%",
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 25,
        marginBottom: 10,
    },
    buttonText: {
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50
    },
    input: {
        width: "80%",
        fontSize: 20,
        height: 44,
        padding: 10,
        borderWidth: 2,
        color: 'black',
        borderRadius: 22,
        borderColor: 'black',
        marginVertical: 10,

    },
    logo: {
        marginTop: "10%",
        width: "90%",
        height: "40%",
        marginLeft: "5%",
        marginRight: "5%",
        marginBottom: "10%",
    },

    multipButtonContiner: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    btnimageWrapper: {
        marginTop: 50,
        width: 200,
        height: 200,
        justifyContent: 'center',
    },
    imageWrapper: {
        width: "100%",
        height: "100%",
        alignItems: 'center',
        resizeMode: 'stretch',
    },
    loginButton: {
        width: "40%",
        height: "20%",
        marginLeft: "5%",
        marginRight: "5%",
    },

    bottomcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: "100%",
        height: 100,
        alignItems: 'center',
       
    },
    buttonContainer: {
        flex: 1,
        height:60,
        justifyContent: 'center',
        bottom: 0,
        borderWidth: 2,
        borderColor: 'white',
        marginLeft: 5,
        marginRight: 5
    }
});
