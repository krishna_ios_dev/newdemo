import React, { Component } from 'react';  
import { StyleSheet,View } from 'react-native';  
  
export default class FlexAlignItems extends Component {  
    render() {  
        return (  
            <View style={FlexAlignItemsStyles.container}>  
                <View style={FlexAlignItemsStyles.powderblue} />  
                <View style={FlexAlignItemsStyles.skyblue} />  
                <View style={FlexAlignItemsStyles.steelblue} />  
            </View>  
        );  
    }  
}  
const FlexAlignItemsStyles = StyleSheet.create({  
    container:{  
        flex: 1,  
        flexDirection: 'column', 
        justifyContent: 'center',  
        alignItems: 'stretch',  
    },  
    powderblue:{  
        width: 60,  
        height: 60,  
        backgroundColor: 'powderblue'  
    },  
    skyblue:{  
        width: 60,  
        height: 60,  
        backgroundColor: 'skyblue',  
    },  
    steelblue:{  
        /*width: 60,*/  
        height: 60,  
        backgroundColor: 'steelblue',  
    }  
})  