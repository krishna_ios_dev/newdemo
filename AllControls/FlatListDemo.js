import React, { Component } from "react";  
import { StyleSheet, View, TextInput, Button, Alert, FlatList , Text} from "react-native";  

export default class FlatListDemo extends Component {
    
    renderSeprator = () => {
        return(
            <View
                style={{
                    height: 1,
                    width:"100%",
                    backgroundColor: "#000"
                }}
            />
        );
    };

    getListViewItem = (item) => {
        Alert.alert(item.key);
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    data={[
                        {key: 'Android'}, {key:'ios'}, {key: 'sap'}
                    ]}
                    renderItem={({item}) =>
                        <Text onPress={this.getListViewItem(this ,item)}>
                            {item.key}
                        </Text>
                    }
                    ItemSeparatorComponent={this.renderSeprator}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,  
        padding: 40,  
        backgroundColor: "#fff",  
        justifyContent: "flex-start"  
    }
})
