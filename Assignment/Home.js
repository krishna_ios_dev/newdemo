import React, { Component } from 'react';
import { Text, Alert, Button, View, StyleSheet, TextInput, TouchableOpacity, Image, ImageBackground, FlatList , Dimensions} from 'react-native';

export default class Home extends Component {

    render() {
        return (
            <View >
                 <ImageBackground
                    source={require('../resources/banner_k.png')}
                    style={styles.imageWrapper}>

                   
                <Text style={styles.buttonText} > Select Your Favourite Social Media Platform</Text>

               <FlatList
                    data={data}
                    renderItem={({item}) => (
                        <View style={styles.itemContainer}>
                        <Text style={styles.item}>{item.value}</Text>
                        <Image style={styles.itemContainer} source={item.value} ></Image>
                        </View>
                    )}
                    keyExtractor={item => item.id}
                    numColumns={numColumns} />

                    <View style={styles.bottomcontainer}>

                        <View style={styles.buttonContainer}>
                            <Button
                                title="Home"
                                style={{ textAlign: 'center' }}
                                color="blue"
                                onPress={() => Alert.alert('Simple Button pressed')}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button style={{ textAlign: 'center'}} 
                                    title="About Us" 
                                    color="red" 
                                    fontWeight="bold"/>
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button style={{ textAlign: 'center' }} 
                                    title="Settings" 
                                    color="green" />
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const data = [
    {id: 'a', value: require('../resources/pintrest.png')},
    {id: 'b', value: require('../resources/insta_logo.png')},
    {id: 'c', value: require('../resources/Snapchat-Splash.png')},
    {id: 'd', value: require('../resources/Splash-Whatsapp-Icon-Png.png')},
    {id: 'e', value: require('../resources/pintrest.png')}, 
  ];
  const numColumns = 2;
  const size = Dimensions.get('window').width/numColumns;
  const styles = StyleSheet.create({
    itemContainer: {
      width: size,
      height: size,
    },
    item: {
      flex: 1,
      margin: 3,
      backgroundColor: 'lightblue',
    },
    buttonText: {
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50
    },
    imageWrapper: {
        width: "100%",
        height: "100%",
        alignItems: 'center',
        resizeMode: 'stretch',
    },
    bottomcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: "100%",
        height: 100,
        alignItems: 'center',
    },
    buttonContainer: {
        flex: 1,
        height:60,
        justifyContent: 'center',
        bottom: 0,
        borderWidth: 2,
        borderColor: 'purple',
        backgroundColor: 'white',
        marginLeft: 5,
        marginRight: 5
    }
  });

