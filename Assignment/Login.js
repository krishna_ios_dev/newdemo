import React, { Component } from 'react';
import { Text, Alert, Button, View, StyleSheet, TextInput, TouchableOpacity, Image, ImageBackground } from 'react-native';

export default class Login extends Component {

    state = {
        email: '',
        password: '',
    };


    onLogin() {
        const { email, password } = this.state;

        Alert.alert('Credentials', `email: ${email} + password: ${password}`);
    }

    render() {
        return (
            <View style={styles.container}>

                <ImageBackground
                    source={require('../resources/banner_k.png')}
                    style={styles.imageWrapper}>

                    <Image style={styles.logo}
                        source={require('../resources/new_login_bg.jpg')}
                        resizeMode="cover"
                    />

                    <TextInput
                        value={this.state.email}
                        keyboardType='email-address'
                        onChangeText={(email) => this.setState({ email })}
                        placeholder='email'
                        placeholderTextColor='black'
                        style={styles.input}
                    />
                    <TextInput
                        value={this.state.password}
                        onChangeText={(password) => this.setState({ password })}
                        placeholder={'password'}
                        secureTextEntry={true}
                        placeholderTextColor='black'
                        style={styles.input}
                    />

                    <Image style={styles.loginButton}
                        source={require('../resources/login_logo.png')}
                        resizeMode="cover"
                    />

                    {/* 
                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.onLogin.bind(this)}
                    >
                        <Text style={styles.buttonText}> Sign Up / Login </Text>
                    </TouchableOpacity> */}

                    <View style={styles.multipButtonContiner}>
                        <TouchableOpacity
                            onPress={() => { alert("handler here") }}>
                            <Image style={styles.btnimageWrapper} source={require('../resources/facebook-Splash-715x715.png')} ></Image>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => { alert("handler here") }}>
                            <Image style={styles.btnimageWrapper} source={require('../resources/Twitter-Splash.png')} ></Image>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    titleText: {
        fontSize: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        alignItems: 'center',
        backgroundColor: 'steelblue',
        width: "80%",
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 25,
        marginBottom: 10,
    },
    buttonText: {
        fontSize: 20,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        width: "80%",
        fontSize: 20,
        height: 44,
        padding: 10,
        borderWidth: 2,
        color: 'black',
        borderRadius: 22,
        borderColor: 'black',
        marginVertical: 10,

    },
    logo: {
        marginTop: "10%",
        width: "90%",
        height: "40%",
        marginLeft: "5%",
        marginRight: "5%",
        marginBottom: "10%",
    },

    multipButtonContiner: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    btnimageWrapper: {
        width: 120,
        height: 120,
        justifyContent: 'center',
    },
    imageWrapper: {
        width: "100%",
        height: "100%",
        alignItems: 'center',
        resizeMode: 'stretch',
    },
    loginButton: {
        width: "40%",
        height: "20%",
        marginLeft: "5%",
        marginRight: "5%",
    },
});