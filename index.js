/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import NativeView from './AllControls/NativeView'
import NativeStates from './AllControls/NativeStates'
import NativeButtons from './AllControls/NativeButtons'
import FlexDirection from './AllControls/FlexDirection' //1
import FlexJustifyContent from './AllControls/FlexJustifyContent'//2
import FlexAlignItems from './AllControls/FlexAlignItems'//3
import FlexCustomProperitsTextButton from './AllControls/FlexCustomProperitsTextButton' //4
import FlexSafeArea from './AllControls/FlexSafeArea' //5
import Flex5 from './AllControls/Flex5' //6
import FlatListDemo from  './AllControls/FlatListDemo'

//Assignment
import Splash from './Assignment/Splash'
import Login from './Assignment/Login'
import Home from './Assignment/Home'

AppRegistry.registerComponent(appName, () => Home);
